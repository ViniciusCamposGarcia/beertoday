//
//  BeertodayTests.swift
//  BeertodayTests
//
//  Created by Vinicius Campos Garcia on 19/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import XCTest
@testable import Beertoday

class BeertodayTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoadEndpointFile(){
        
        var success = true
        let properties = Request.Requests().getpropriedadesBarramento()
        if properties.keys.contains("URL") == false{
            success = false
        }
        
        XCTAssert(success)
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
