//
//  Connection.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 19/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Alamofire

class Connection: NSObject {
    
    class func isActive() -> Bool{
        
        // Verifica se tem acesso a internet via Wifi ou Dados Móveis

        
        let reachability = Reachability(hostname: "http://www.google.com.br")
            
        if let rec = reachability{
            
            return rec.isReachable
        }else{
            return true
        }
    }
    
    class func confirm(completion : @escaping (Bool) -> Void){
        
        Alamofire.request("www.google.com.br").responseJSON { (response) in
            completion(response.result.isSuccess)
        }
    }
}
