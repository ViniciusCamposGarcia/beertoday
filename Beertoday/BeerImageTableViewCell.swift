//
//  BeerImageTableViewCell.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 23/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit

class BeerImageTableViewCell: UITableViewCell {

    
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageViewProduto: UIImageView!
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 0 {
            containerView.clipsToBounds = true
            // scrolling up
            bottomSpaceConstraint?.constant = -scrollView.contentOffset.y / 2
            topSpaceConstraint?.constant = (scrollView.contentOffset.y / 2) + 20
        }else{
            scrollView.contentOffset = CGPoint.zero
            topSpaceConstraint?.constant = scrollView.contentOffset.y + 20
            containerView.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
