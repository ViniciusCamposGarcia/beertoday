//
//  Request.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 19/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit
import Alamofire

typealias BTRequest = (URL : String, method : Alamofire.HTTPMethod, timeout : Int)
typealias BTReturnMessage = (title : String, message : String)

class Request: NSObject {
    
    
    //MARK: - Saida de todas as requisicoes HTTP da aplicacao
    
    /**
     Método principal de saida das requisições HTTP.
     
     - parameter method: Um método listado na classe alamofire referente ao tipo de requicisão HTTP.
     - parameter url: O endereço web a ser requisitado.
     - parameter parameters: O body da requisição.
     - parameter encoding: Encoding dos textos enviados.
     - parameter headers: Os headers da requisição.
     - parameter responseJson: Uma closure que será chamada quando a requisição for concluida.
     */
    
    
    func request(forBTRequest btRequest: BTRequest, parameters: [String : AnyObject]? = nil, inSuccess : @escaping (AnyObject?) -> Void, inFailure iF: @escaping (_ statusCode : Int?) -> Void){
        
        
        func request(method m: Alamofire.HTTPMethod, url : String, param : [String : AnyObject]?, headers : [String : String]?){
            
            
            Alamofire.request(url, method: m, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                
                
                if response.result.isSuccess{
                    
                    if response.response?.statusCode == 200{
                    
                        inSuccess(response.result.value as AnyObject)

                    }else{
                    
                        iF(response.response?.statusCode)
                    
                    }
                
                    
                }else{
                    
                    iF(response.response?.statusCode)
                }
            }
        }
        
        //Inicio da funcao
        
        var url = btRequest.URL
        url = url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
        
        do {
        
            let _ = try (url as URLConvertible).asURL()
        
            // Verifica se tem acesso a internet via Wifi ou Dados Móveis
            
            if Connection.isActive(){
                
                request(method: btRequest.method, url: url, param: parameters, headers: nil)
                
            }else{
                
                iF(0)
            }
        }catch{
            
            iF(0)
        }
    }
    
    public class Requests{
        
        //MARK: - Configuration
        
        func getpropriedadesBarramento() -> [String : AnyObject]{
            
            var myDict: [String : AnyObject]?
            if let path = Bundle.main.path(forResource: "Endpoint", ofType: "plist") {
                myDict = NSDictionary(contentsOfFile: path) as? [String : AnyObject]
            }
            return myDict!
        }
        
        private func getURL() -> String{
            let propriedadesBarramento = self.getpropriedadesBarramento()
            let URL = propriedadesBarramento["URL"] as! String
            return URL
        }
    
        //MARK: - Requests/Beers
        
        func GETBeers(query : String) -> BTRequest{
            return ( URL : self.getURL().appending(query), method: .get, timeout : 20)
        }
        
        func GETRamdomBeer() -> BTRequest{
            return ( URL : self.getURL().appending("beers/random"),  method: .get, timeout : 20)
        }
    }
}
