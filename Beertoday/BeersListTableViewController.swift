//
//  BeersListTableViewController.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 19/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit

class BeersListTableViewController: UITableViewController {
    
    var beers : [BeerDTO]?
    var AllLoaded : Bool = false
    
    @IBOutlet weak var loadingBeersActivityView : UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.btBrown
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.loadBeers()
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.beers?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseBeersTableCell", for: indexPath) as! BeerTableViewCell
        
        cell.beer = beers![indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let bs = self.beers{
            
            if (indexPath.row > (bs.count - 5)) && (AllLoaded == false){
            
                
            }
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier ?? "") == Segues.BeersListShowBeer{
        
            if let snd = sender, snd is BeerTableViewCell ,segue.destination is BeerDetailTableViewController{
            
                let indexRow = self.tableView.indexPath(for: snd as! BeerTableViewCell)?.row
                
                (segue.destination as! BeerDetailTableViewController).beerId = self.beers?[indexRow!].id
            }
        }
    }
    
    
    func loadBeers(page : Int? = nil){
        
        loadingBeersActivityView.startAnimating()
        loadingBeersActivityView.isHidden = false
        
        
        Beers.get{ (beerRequestResult) in
            
            self.loadingBeersActivityView.stopAnimating()
            self.loadingBeersActivityView.isHidden = true
            
            if beerRequestResult.success{
            
                
                if page == nil{
                    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
                }
                
                self.beers = beerRequestResult.beers
                self.tableView.reloadData()
            
            }else{
            
            
                let alert = UIAlertController(title: "Ops", message: beerRequestResult.message, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tentar Novamente", style: UIAlertActionStyle.default, handler: { (action) in
                    
                    self.loadBeers()
                    
                }))
                alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: { (alert) in
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
}

extension Segues{
    
    static var BeersListShowBeer: String{
        return "BeersListShowBeer"
    }
}
