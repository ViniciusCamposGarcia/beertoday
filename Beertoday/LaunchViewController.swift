//
//  LaunchViewController.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 25/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var xImageConstraint : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.xImageConstraint.constant = self.view.center.x - (imageView.frame.width / 2)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.animar()
        }
    }
    
    private func animar(){
        
        let duration = 0.7
        let rotation = 4.5
        
        let rotationAnimation : CABasicAnimation
        rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: Float((Double.pi * 2.0) * Double(rotation) * Double(duration)))
        rotationAnimation.duration = duration
        rotationAnimation.isCumulative = true
        rotationAnimation.repeatCount = 3.0
        
        
        self.imageView.layer.add(rotationAnimation, forKey: "rotationAnimation")
        
        
        self.view.setNeedsUpdateConstraints()
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.xImageConstraint.constant = self.view.frame.width + self.imageView.frame.width
            self.view.layoutIfNeeded()
        }) { _ in
            self.prosseguir()
        }
    }
    
    private func prosseguir(){
        
        for view in self.view.subviews{
            let eachView = view
            eachView.isHidden = true
        }
        
        let rootViewController: UINavigationController = self.storyboard?.instantiateViewController(withIdentifier: "RootController") as! UINavigationController
        
        let keyWindow = UIApplication.shared.keyWindow
        
        keyWindow?.rootViewController = rootViewController
        keyWindow?.makeKeyAndVisible()
        
        let viewAnimacao: UIView = UIView(frame: rootViewController.view.frame)
        viewAnimacao.backgroundColor = UIColor.white
        
        rootViewController.view.addSubview(viewAnimacao)
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            viewAnimacao.alpha = 0
        }) { (Bool) -> Void in
            viewAnimacao.removeFromSuperview()
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
