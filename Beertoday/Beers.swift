//
//  Beers.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 19/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit

class Beers: NSObject {

    class func get(completion : @escaping (BeerRequestResult) -> Void){
        
        let result : BeerRequestResult = BeerRequestResult()
        let request = Request.Requests().GETBeers(query: "beers")
        
        Request().request(forBTRequest: request, inSuccess: { (returnedObject) in
            
            result.success = true
            
            if let rO = returnedObject, rO is [[String : AnyObject]]{
                
                for object in (rO as! [[String : AnyObject]]){
                    
                    if let beerObject = BeerDTO(JSON: object){
                        
                        if result.beers == nil{
                            result.beers = [BeerDTO]()
                        }
                        
                        result.beers!.append(beerObject)
                    }
                }
            }
            
            completion(result)
        
        }) { (statusCode) in
            
            switch statusCode ?? 999{
            case 0:
                result.message = "Sem Conexao com a internet"
                break
            case 1:
                result.message = "Falha ao realizar requisicao"
                break
            case 429:
                result.message = "Parece que esta usando muito o app, quantidade de requisicoes ultrapassadas"
                break
            default:
                result.message = "Falha Interna"
                break
            }
            
            completion(result)
        }
    }
    
    class func get(id i: Int, completion : @escaping (BeerRequestResult) -> Void){
        
        let result : BeerRequestResult = BeerRequestResult()
        let request = Request.Requests().GETBeers(query: "beers/\(i)")
    
        Request().request(forBTRequest: request, inSuccess: { (returnedObject) in
            
            result.success = true
            
            if let rO = returnedObject, rO is [[String : AnyObject]]{
                
                for object in (rO as! [[String : AnyObject]]){
                    
                    if let beerObject = BeerDTO(JSON: object){
                        
                        if result.beers == nil{
                            result.beers = [BeerDTO]()
                        }
                        
                        result.beers!.append(beerObject)
                    }
                }
            }
            
            completion(result)
            
        }) { (statusCode) in
            
            switch statusCode ?? 999{
            case 0:
                result.message = "Sem Conexao com a internet"
                break
            case 1:
                result.message = "Falha ao realizar requisicao"
                break
            case 429:
                result.message = "Parece que esta usando muito o app, quantidade de requisicoes ultrapassadas"
                break
            default:
                result.message = "Falha Interna"
                break
            }
            
            completion(result)
        }
    }
}
