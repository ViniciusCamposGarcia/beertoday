//
//  BeerDetailTableViewController.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 22/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit

class BeerDetailTableViewController: UITableViewController {

    var beerId : Int?
    
    @IBOutlet weak var loadingActivityIndicator : UIActivityIndicatorView!
    
    @IBOutlet weak var beerImageView : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var taglineLabel : UILabel!
    @IBOutlet weak var abvLabel : UILabel!
    @IBOutlet weak var ibuLabel : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.loadBeerDetails()
    }
    
    func loadBeerDetails(){
    
        if let id = self.beerId{
        
            Beers.get(id: id, completion: { (beerRequestResult) in
                
                self.loadingActivityIndicator.stopAnimating()
                
                if beerRequestResult.success{
                
                    if let beers = beerRequestResult.beers, beers.count > 0{
                        
                        self.setBeerValues(beer: beers[0])
                    
                    }
                }else{
                
                
                }
            })
        }
    }

    func setBeerValues(beer : BeerDTO){
        
        self.beerImageView.setImage(with: beer.imageUrl) { (sucesso) in
            
        }
    
        self.nameLabel.text = beer.name
        self.abvLabel.text = beer.abv != nil ? "\(beer.abv!)%" : ""
        self.ibuLabel.text = beer.ibu != nil ? String(beer.ibu!) : ""
        self.taglineLabel.text = beer.tagline
        self.descriptionLabel.text = beer.beerDescription
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.tag == 0{
            
            if let imageCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? BeerImageTableViewCell {
                imageCell.scrollViewDidScroll(scrollView: scrollView)
            }
        }
    }

}
