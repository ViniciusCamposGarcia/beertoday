//
//  BeerTableViewCell.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 21/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit
import SDWebImage

class BeerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewBeer: UIImageView!
    @IBOutlet weak var labelAlcoholByVolume: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var activityLoadImage: UIActivityIndicatorView!
    
    var _beer : BeerDTO?
    var beer : BeerDTO? {
        get{
            return _beer
        }
        set{
            _beer = newValue
            
            self.activityLoadImage.isHidden = false
            self.activityLoadImage.startAnimating()
            
            if let value = newValue{
                
                
                imageViewBeer.setImage(with: value.imageUrl, completion: { (sucesso) in
                        
                    self.activityLoadImage.stopAnimating()

                })
                    
                self.labelName.text = value.name ?? ""
                self.labelAlcoholByVolume.text = value.abv !=  nil ? "\(value.abv!)%" : "--"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
