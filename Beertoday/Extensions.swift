//
//  Extensions.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 22/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit

class Extensions: NSObject {

}

extension UIColor{

    static var btBrown: UIColor {
        return UIColor(red: 78/255.0, green: 46/255.0, blue: 2/255.0, alpha: 1.0)
    }
}
