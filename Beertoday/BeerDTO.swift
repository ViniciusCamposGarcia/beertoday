//
//  BeerDTO.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 19/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class BeerMeasure : NSObject{
    
    var value : Double?
    var unit : String?
    
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        
        self.value          <- map["value"]
        self.unit           <- map["unit"]
    }
}

class BeerTime : NSObject{
    
    var temp : BeerMeasure?
    var duration : String?
    
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        
        self.temp           <- map["temp"]
        self.duration       <- map["duration"]
    }
}

class BeerMethod : NSObject{
    
    var mashTemp : [BeerTime]?
    var fermentation : BeerTime?
    var twist : NSObject?
    
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        
        self.mashTemp           <- map["mash_temp"]
        self.fermentation       <- map["fermentation"]
        self.twist              <- map["twist"]
    }
}

class BeerIngredient : NSObject{
    
    var malt : [BeerIngredientItem]?
    var hops : [BeerIngredientItem]?
    var yeast : String?
    
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        
        self.malt               <- map["malt"]
        self.hops               <- map["hops"]
        self.yeast              <- map["yeast"]
    }
}

class BeerIngredientItem : NSObject{
    
    var name : String?
    var amount : BeerMeasure?
    var add : String?
    var attribute : String?
    
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        
        self.name               <- map["name"]
        self.amount             <- map["amount"]
        self.add                <- map["add"]
        self.attribute          <- map["attribute"]
    }
}

class BeerDTO: NSObject, Mappable {
    
    var id : Int?
    var name : String?
    var tagline : String?
    var firstBrewed : String?
    var beerDescription : String?
    var imageUrl : String?
    var abv : Float?
    var ibu : Int?
    var targetFg : Int?
    var targetOg : Int?
    var ebc : Int?
    var srm : Int?
    var ph : Float?
    var attenuationLevel : Float?
    var volume : BeerMeasure?
    var boilVolume : BeerMeasure?
    var method : BeerMethod?
    var ingredients : BeerIngredient?
    var foodPairing : [String]?
    var brewersTips : String?
    var contributedBy : String?
    
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        
        self.id                 <- map["id"]
        self.name               <- map["name"]
        self.tagline            <- map["tagline"]
        self.firstBrewed        <- map["first_brewed"]
        self.beerDescription    <- map["description"]
        self.imageUrl           <- map["image_url"]
        self.abv                <- map["abv"]
        self.ibu                <- map["ibu"]
        self.targetFg           <- map["target_fg"]
        self.targetOg           <- map["target_og"]
        self.ebc                <- map["ebc"]
        self.srm                <- map["srm"]
        self.ph                 <- map["ph"]
        self.attenuationLevel   <- map["attenuation_level"]
        self.volume             <- map["volume"]
        self.boilVolume         <- map["boil_volume"]
        self.method             <- map["method"]
        self.ingredients        <- map["ingredients"]
        self.foodPairing        <- map["food_pairing"]
        self.brewersTips        <- map["brewers_tips"]
        self.contributedBy      <- map["contributed_by"]
    }
}


