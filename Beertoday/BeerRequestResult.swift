//
//  BeerResult.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 22/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit

class BeerRequestResult: RequestResult{
    
    var beers : [BeerDTO]?
    var page : Int = 0
}
