//
//  ImageCaching.swift
//  Beertoday
//
//  Created by Vinicius Campos Garcia on 22/06/17.
//  Copyright © 2017 Movini Solutions. All rights reserved.
//

import UIKit
import SDWebImage

class ImageCaching: NSObject {

}

extension UIImageView{

    
    func setImage(with urlString: String?, completion : @escaping (_ sucesso : Bool) -> Void){
    
        if let url = urlString, URL(string: url) != nil{
        
            self.sd_setImage(with: URL(string: url), completed: { (image, error, cacheType, url) in
                
                completion(error == nil)
            })
        }else{
            completion(false)
        }
    }
}
